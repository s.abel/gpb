#!/usr/bin/env perl
use warnings;
use strict;
use Getopt::Long;
use DBI;

# TODO: make configurable via external config file
my $DB_CONN = "DBI:mysql:database=xxx;host=localhost;port=3306";
my $DB_USER = 'xxx';
my $DB_PASSWORD = 'xxx';

my $usage = <<EOT;
USAGE: $0 <STDIN>
Parse log file and record data in the database. 
E.g. :
\$ cat out | perl $0
EOT

my $SEPARATOR = ' ';
my $INBOUND = 'in';
my $stub = 1;
my %FLAG = (
    '<=' => $INBOUND,
    '=>' => $stub,
    '->' => $stub,
    '**' => $stub,
    '==' => $stub,
);

my $MESSAGE_ID_RE = qr/id=(\S+)/;

GetOptions(
    help => \&print_usage,
);

sub print_usage {
    print $usage;
    exit(0);
}

main();

sub main {
    my $dbh = get_dbh() or crash("Failed to connect. Open $0 and fix DB connection details");
    
    my $sth_msg = $dbh->prepare(<<SQL) or die $dbh->errstr;
        INSERT INTO message (created, id, int_id, str) VALUES (?, ?, ?, ?)
SQL

    my $sth_log = $dbh->prepare(<<SQL) or die $dbh->errstr;
        INSERT INTO log (created, address, int_id, str) VALUES (?, ?, ?, ?)
SQL

    # TODO: For performance: use multi-row inserts, drop and re-create table indexes?

    while (my $line = <STDIN>) {
        chomp $line;
        my ($date, $time, $rest) = split($SEPARATOR, $line, 3);
        my ($int_id, $flag, $addr, $extra) = split($SEPARATOR, $rest, 4);
        my $created = "$date $time";
        my $dir = $FLAG{$flag};
        if ($dir and $dir eq $INBOUND) {
            my ($id) = $extra =~ m/$MESSAGE_ID_RE/;
            # XXX: id is a NON NULL, so can't insert without it
            unless ($id) {
                warn "Failed to extract id from $int_id";
                next;
            }
            $sth_msg->execute($created, $id, $int_id, $rest)
                or warn "Failed to insert message $int_id: "  . $sth_msg->errstr;
        } else {
            $sth_log->execute($created, $addr, $int_id, $rest)
                or warn "Failed to insert log $int_id: "  . $sth_log->errstr;
        }
    }
    $dbh->commit;
    return 1;
}

sub crash {
    my $msg = shift;
    print $msg, "\n";
    exit(1);
}

# TODO: make a lib function
sub get_dbh {
    return DBI->connect($DB_CONN, $DB_USER, $DB_PASSWORD, {
        RaiseError => 0,
        PrintError => 0,
        AutoCommit => 0, # disabled for performance
    });
}

__END__

This script uses some optimisations for performance (and lists TODOs to make it more performant),
because log files can grow very large. It reads from standard input like some standanrd Linux utils do.

