#!/usr/bin/env perl
use warnings;
use strict;
use DBI;
use HTTP::Daemon;
use HTTP::Status;
use HTTP::Headers;
use URI;

# TODO: make configurable via external config file
my $DB_CONN = "DBI:mysql:database=xxx;host=localhost;port=3306";
my $DB_USER = 'xxx';
my $DB_PASSWORD = 'xxx';
my $LIST_SIZE = 100;

# TODO: implement DB re-connect
my $dbh; 

sub show_page {
    my $req = shift;
    my %params = URI->new($req->uri)->query_form;
    my $results = '';
    if (length $params{address}) {
        $results = lookup_address($params{address});
    }

    # XXX: putting HTML inside a script like that is a bad practice
    my $html = <<HTML;
<html>
<head>
</head>
<body>
<form>
<input type="text" name="address" id="address">
<button type="submit">Search</button>
<hr />
$results
</form>
</body>
</html>
HTML
    return $html;
}

sub lookup_address {
    my $address = shift;

    # Tested on a MySQL V5.7
    my $rows = $dbh->selectall_arrayref(<<SQL, { Slice => {}}, $address, $address, $LIST_SIZE + 1) || die $dbh->errstr;
        SELECT created, int_id
        FROM log
        WHERE int_id IN (
            SELECT int_id FROM log WHERE address = ?
        )
        UNION ALL
        SELECT created, int_id
        FROM message
        WHERE int_id IN (
            SELECT int_id FROM log WHERE address = ?
        )
        ORDER BY int_id ASC, created ASC
        LIMIT ?
SQL

    # The last one indicates a second page
    my $has_more = @$rows > $LIST_SIZE
        ? !!shift(@$rows)
        : undef;

    # XXX: Again, this code looks like it was written 20 years ago! But I don't want to use a template engine 
    # for a small script like that
    my $html = join "\n", map { "<p>&lt;$_->{created}&gt; $_->{int_id}</p>" } @$rows;
    my $more_info = $has_more
        ? "<p>More than $LIST_SIZE items found</p>"
        : '';
    return <<HTML;
    $more_info
    $html
HTML
}

main();

sub main {
    # TODO: Put server logic in a separate module
    my $d = HTTP::Daemon->new(
        Reuse => 1
    ) || die;
    print "Please contact me at: <URL:", $d->url, ">\n";
    connect_db();
    while (my $c = $d->accept) {
        while (my $r = $c->get_request) {
            # TODO: Implement a proper, maintainable route dispatcher
            if ($r->method eq 'GET' and $r->uri->path eq "/") {
                my $headers = HTTP::Headers->new(
                    Content_Type => 'text/html'
                );
                # TODO: Implement a proper error handler
                my $page = eval { show_page($r) } || "Error $@";
                $c->send_response(200, 'OK', $headers, $page); 
            } else {
                $c->send_error(RC_FORBIDDEN)
            }
        }
        $c->close;
        undef($c);
    }
    return 1;
}


# TODO: make a lib function
sub connect_db {
    $dbh = DBI->connect($DB_CONN, $DB_USER, $DB_PASSWORD, {
        RaiseError => 0,
        PrintError => 0,
        AutoCommit => 0,
    });
}

__END__

Note, this code uses some bad practices. But it's done that way with a knowledge of that. Comments
were added to highlight those practices. The benefit is that it doesn't require any web framewors or external tools
and runs on a standard Perl installation.

